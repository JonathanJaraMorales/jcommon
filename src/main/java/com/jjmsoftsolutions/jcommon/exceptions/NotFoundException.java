package com.jjmsoftsolutions.jcommon.exceptions;

import com.jjmsoftsolutions.jcommon.generic.ResponseStatus;

public class NotFoundException extends JException { 
	
	private static final long serialVersionUID = 1L;
	private int code;
	
	public NotFoundException() {
		this("There is not any records with the specified criteria", ResponseStatus.NOT_FOUND.getStatus());
	}
	
	public NotFoundException(String message, int code){
		super(message);
		this.code = code;
	}

	@Override
	public int getCode(){
		return code;
	}
	
}
