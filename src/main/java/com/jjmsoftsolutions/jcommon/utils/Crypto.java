package com.jjmsoftsolutions.jcommon.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;

import org.apache.commons.codec.binary.Base64;

public class Crypto {

	private static final byte[] SALT = { (byte) 0x21, (byte) 0x21, (byte) 0xF0, (byte) 0x55, (byte) 0xC3, (byte) 0x9F, (byte) 0x5A, (byte) 0x75 };
	private final static int ITERATION_COUNT = 31;
	
	public static String md5(String text) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] b = md.digest(text.getBytes());

			int size = b.length;
			StringBuffer h = new StringBuffer(size);
			for (int i = 0; i < size; i++) {
				int u = b[i] & 255;
				if (u < 16) {
					h.append("0" + Integer.toHexString(u));
				} else {
					h.append(Integer.toHexString(u));
				}
			}
			return h.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
		
	}

	public static String encode(String input) {
		if (input == null) {
			throw new IllegalArgumentException();
		}
		KeySpec keySpec = new PBEKeySpec(null, SALT, ITERATION_COUNT);
		AlgorithmParameterSpec paramSpec = new PBEParameterSpec(SALT, ITERATION_COUNT);

		try {
			SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
			Cipher ecipher = Cipher.getInstance(key.getAlgorithm());
			ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);

			byte[] enc = ecipher.doFinal(input.getBytes());

			String res = new String(Base64.encodeBase64(enc));
			res = res.replace('+', '-').replace('/', '_').replace("%", "%25").replace("\n", "%0A");
			return res;
		} catch (Exception e) {
			e.printStackTrace();
		} 
		return null;
		
	}

	public static String decode(String token) {
		if (token == null) {
			return null;
		}
		String input = token.replace("%0A", "\n").replace("%25", "%").replace('_', '/').replace('-', '+');

		byte[] dec = Base64.decodeBase64(input.getBytes());

		KeySpec keySpec = new PBEKeySpec(null, SALT, ITERATION_COUNT);
		AlgorithmParameterSpec paramSpec = new PBEParameterSpec(SALT, ITERATION_COUNT);

		SecretKey key;
		try {
			key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);
			Cipher dcipher = Cipher.getInstance(key.getAlgorithm());
			dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
			byte[] decoded = dcipher.doFinal(dec);
			String result = new String(decoded);
			return result;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

		
	}

}
