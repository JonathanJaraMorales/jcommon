package com.jjmsoftsolutions.jcommon.json.factory;

import javax.inject.Inject;
import javax.inject.Named;

import com.jjmsoftsolutions.jcommon.json.JSonConverter;

@Named
public class JSonFactoryConverter<T> {
	
	@Inject private JSonConverter<T> converter;
	
	public JSonConverter<T> getInstance(){
		return converter;
	}

}
