package com.jjmsoftsolutions.jcommon.json;

import javax.inject.Named;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import com.google.gson.Gson;

@Named
public class GsonConverter<T> implements JSonConverter<T> {

	public T cast(Class<T> clazz, String json) {
		//return Fixjure.of(clazz).from(JSONSource.newJsonString(json)).create();
		Gson gson = new Gson();
		return gson.fromJson(json, getClassDeserializable(clazz));
	}
	
	public String getJSon(Object object) {
		Gson gson = new Gson();
		return gson.toJson(object);
	}

	@SuppressWarnings("unchecked")
	public Class<T> getClassDeserializable(Class<T> clazz) {
		Class<T> clazzImpl = null;
		if(clazz.isAnnotationPresent(JsonDeserialize.class)){
			JsonDeserialize annotation = clazz.getAnnotation(JsonDeserialize.class);
			JsonDeserialize info = (JsonDeserialize) annotation;
			clazzImpl = (Class<T>) info.as();
		}
		return clazzImpl;
	}

	
}
