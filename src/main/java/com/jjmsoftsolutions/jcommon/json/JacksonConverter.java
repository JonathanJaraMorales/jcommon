package com.jjmsoftsolutions.jcommon.json;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class JacksonConverter<T> implements JSonConverter<T>{
	
	public T cast(Class<T> clazz, String json) throws ClassNotFoundException {
		Class<?> gsonBuilderClass = null;
		Method[] methods = clazz.getDeclaredMethods();
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = null;
		for(Method method : methods){
			Class<?> clazzMethod = method.getReturnType();
			if(clazzMethod.isInterface()){
				if(clazzMethod == List.class){
					Type type = method.getGenericReturnType();
					if(type instanceof ParameterizedType){
						Type[] types = ((ParameterizedType) type).getActualTypeArguments();
						for(Type paramTypes: types){
							gsonBuilderClass = Class.forName(paramTypes.getTypeName());
						}
					}
				}	
			}
			gsonBuilderClass = clazzMethod;
			gsonBuilder.registerTypeAdapter(gsonBuilderClass, new InterfaceAdapter<Object>());	
		}
		gson = gsonBuilder.create();
		return gson.fromJson(json, clazz);
	}

	public String getJSon(T t) {
		Gson gson = new Gson();
		return gson.toJson(t);
	}

	@SuppressWarnings("unchecked")
	public Class<T> getClassDeserializable(Class<T> clazz) {
		Class<T> clazzImpl = null;
		if(clazz.isAnnotationPresent(JsonDeserialize.class)){
			JsonDeserialize annotation = clazz.getAnnotation(JsonDeserialize.class);
			JsonDeserialize info = (JsonDeserialize) annotation;
			clazzImpl = (Class<T>) info.as();
		}
		return clazzImpl;
	}

}
