package com.jjmsoftsolutions.jcommon.json;

public interface JSonConverter<T> {
	public T cast(Class<T> clazz, String json) throws ClassNotFoundException;
	public String getJSon(T t);
	public Class<T> getClassDeserializable(Class<T> clazz);
}
