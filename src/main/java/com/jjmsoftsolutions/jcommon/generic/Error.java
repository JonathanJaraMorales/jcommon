package com.jjmsoftsolutions.jcommon.generic;

public enum Error {
	
	USER_INSERT(1, "The user exist"),
	USER_FIND_BY_EMAIL_PASSWORD(2, "The user doesn't exist"),
	
	POST_FIND_BY_ID(3,"The post doesn't exist"),
	POST_FIND_BY_LANGUAGE(4, "There aren't any post with the selected languge"),
	POST_INSERT(5,"Imposible create the post"),
	POST_INSERT_VALIDATION_LANGUAGE(14, "Imposible create the post, please send a validate language"),
	POST_COUNT_BY_LANGUAGE(6,""),
	POST_FIND_BY_TAG(7,""),
	POST_COUNT_BY_TAG(8, ""),
	POST_FIND_BY_TAG_ID(9,""),
	POST_DELETE(10, ""),
	POST_SEARCH_ENGINE(11,""),
	POST_FIND_BY_LANGUAGE_SUPER_POST(12, ""),
	POST_COUNT_BY_SEARCH_ENGINE(13, "");
	
	private final int code;
	private final String message;

	Error(final int statusCode, final String message) {
		this.code = statusCode;
		this.message = message;

	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}
