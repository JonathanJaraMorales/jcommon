package com.jjmsoftsolutions.jcommon.generic;

public enum ResponseStatus {

	CONFLICT(409, "Conflict"),
	NOT_FOUND(404, "Not Found"),
	OK(200, "OK");

	private final int code;
	private final String message;

	ResponseStatus(final int statusCode, final String message) {
		this.code = statusCode;
		this.message = message;

	}

	public int getStatus() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
